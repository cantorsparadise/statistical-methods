---
title: "R's Notebook"
output: html_notebook
---

```{r, echo = FALSE, message = FALSE}
packages_needed <- c("tidyverse", "devtools", "lme4", 
                     "lattice", "lmtest", "randomNames",
                     "ggforce", "ggthemes", "showtext")

package.check <- lapply(
  packages_needed,
  FUN = function(x) {
    if (!require(x, character.only = TRUE)) {
      install.packages(x, dependencies = TRUE, 
      repos = "https://cloud.r-project.org/") 
    }
  }
)

# Remove objects no longer needed
rm(packages_needed, package.check)
```
# Rejection Sampling

 rejection sampling is a  brute force   technique for sampling from a density function.
  to illustrate the principal, suppose that we would like to estimate the area of a circle $A_C$. To this and, we may compare the unknown area with a known area, such as that of a square $A_S = \ell^2$.
  in practice, this cuold be realized by uniformly throwing darts at a rectangular board  enclosing the circle. Accordingly, the proportion of darts $p$ within the circular region yields an approximation to the circular area,
$$A_C \approx pA_S.$$
```{r}
#
# Simulation Parameters
#

circle_r <- 1
n <- 1000

#
# Pefrorm simulation
#

simulation <- tibble(
  x = runif(n, min = -circle_r, max = circle_r),
  y = runif(n, min = -circle_r, max = circle_r),
  Reject = as.logical(x^2 + y^2 <= circle_r)
)

library(showtext)
font_add_google("Source Sans Pro", family = "special")
font_add_google("Work Sans", family = "special2")

showtext_auto()

ggplot(simulation) +
  geom_point(aes(x = x, y = y, alpha = Reject, color = Reject)) +
  scale_color_manual(values = c("#f7c173", "#7fb1de")) +
  geom_rect(aes(xmin = -circle_r, xmax = circle_r, ymin = -circle_r, ymax = circle_r), fill = NA, color = "#dfa944", alpha = 0.5) +
  geom_circle(aes(x0 = 0, y0 = 0, r = circle_r), color = "#254151") +
  coord_fixed() +
  theme_stata(scheme = "s1manual") +
  labs(
    title = "Sampling the Area of a Circle"
  ) +
  theme(
    title = element_text(family = "special"),
    axis.title.x = element_text(family = "special"),
    axis.text.x = element_text(family = "special2"),
    axis.text.y = element_text(family = "special2"),
    legend.text = element_text(family = "special2"),
    legend.title = element_text(family = "special")
  )
```


# Example: Rejection Sampling From a Beta

```{r}
#
# Simulation parameters
#

# Define the beta parameters
alpha <- 2; beta <- 2
proposal_n <- 10; range_n <- 100

# The proposal distribution is uniform, coinciding with the beta mode
a <- 0; b <- dbeta((2 - 1)/(2 + 2 - 2), alpha, beta)

# Thus, the likelihood ratio is unity
L <- 1 / b

#
# Perform rejection sampling
#

proposal_sample <- rbeta(proposal_n, alpha, beta)

simulation <- tibble(
  x = rep(proposal_sample, times = range_n),
  y = runif(proposal_n * range_n, min = 0, max = b),
  contains = as.logical(y <= 1/(L * dunif(x, min = 0, max = b)) * dbeta(x, alpha, beta))
)

ggplot(simulation, aes(x = x, y = y)) +
  geom_point(aes(alpha = contains)) +
  geom_function(fun = dbeta, args = list(shape1 = alpha, shape2 = beta)) +
  geom_function(fun = dunif, args = list(min = 0, max = b)) 
```

